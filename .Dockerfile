FROM node:latest

WORKDIR /server

COPY /package.json .

RUN npm install

COPY ./server .

EXPOSE 3000 3307 5673

CMD [ "npm", "start" ]