'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Engines', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      emissieNorm: {
        type: Sequelize.STRING
      },
      emissionClass: {
        type: Sequelize.STRING
      },
      driveType: {
        type: Sequelize.STRING
      },
      power: {
        type: Sequelize.INTEGER
      },
      fuel: {
        type: Sequelize.STRING
      },  
      hasSoothFilter: {
        type: Sequelize.BOOLEAN
      },
      vehicle_id: {
        type: Sequelize.INTEGER,
        references: {
            model: 'Vehicles',
            key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Engines');
  }
};