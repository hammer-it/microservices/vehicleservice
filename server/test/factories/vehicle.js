
const faker = require('faker');
const models = require('../../models');
/**
 * Generate an object which container attributes needed
 * to successfully create a vehicle instance.
 * 
 * @param  {Object} props Properties to use for the vehicle.
 * 
 * @return {Object}       An object to build the vehicle from.
 */
const vehicleData = async (props = {}) => {
  const defaultProps = {
    id: 42069,
    vehicleId: faker.name.lastName(),
    vehicledescription: faker.name.firstName(),
    vehicleType: faker.vehicle.type(),
    brand: faker.vehicle.manufacturer(),
    model: faker.vehicle.model(),
    modelYear: 2012,
    weight: 20
  };
  return Object.assign({}, defaultProps, props);
};
const PrimaryEngineData = async (props = {}) => {
  const defaultProps = {
    id: 1337,
    emissieNorm: faker.name.firstName(),
    emissionClass: faker.name.lastName(),
    driveType: faker.vehicle.type(),
    power: 50,
    fuel: faker.vehicle.fuel(),
    hasSoothFilter: false,
    vehicleId: 42069
  };
  return Object.assign({}, defaultProps, props);
};

const SecundaryData = async (props = {}) => {
  const defaultProps = {
    id: 1338,
    emissieNorm: faker.name.firstName(),
    emissionClass: faker.name.lastName(),
    driveType: faker.vehicle.type(),
    power: 50,
    fuel: faker.vehicle.fuel(),
    hasSoothFilter: false,
    vehicleId: 42069
  };
  return Object.assign({}, defaultProps, props);
};

/**
 * Generates a vehicle instance from the properties provided.
 * 
 * @param  {Object} props Properties to use for the vehicle.
 * 
 * @return {Object}       A vehicle instance
 */
module.exports = async (props = {}) =>
  models.Vehicle.create(await vehicleData(props));
