const map = require('lodash/map');
const models = require('../models');

/**
 * This will iterate through all of your models, and delete any 
 * data out of the database associated with those models
 */
module.exports  = async function truncate() {
  return await Promise.all(
    map(Object.keys(models), (key) => {
      if (['sequelize', 'Sequelize'].includes(key)) return null;
      return models[key].destroy({ where: {}, force: true });
    })
  );
};
