const express = require('express');
const router = express.Router();
const { Vehicle } = require('../models');
const { Engine } = require('../models');
 const RabbitMQMessagingService = require('../rabbitMQ');

 const vehicleService = new RabbitMQMessagingService('vehicles');
 const engineService = new RabbitMQMessagingService('engines');
vehicleService.connect();
engineService.connect();

// Test
router.get('/', async (req, res, next) => {
    res.send("Test Completed!");
});
router.get('/:companyId', async (req, res, next) => {
    const companyId = req.params.companyId;
    Vehicle.findAll({where: {company_id: companyId}}).then((vehciles) => {
        res.send(vehciles);
    }).catch(err  => {
        console.log(err);
        //res.sendStatus(err)
        try{
            let temp = vehicleService.backUpQueue();
            console.log('got back-up');
            console.log(temp);
        }catch(err){

        }
    })
});

// Get a specific vehicle
router.get('/:companyId/:vehicleId', async (req,res,next) => {
    const companyId = req.params.companyId;
    const vehicleId = req.params.vehicleId;
    console.log(vehicleId)
    Vehicle.findAll({where: {company_id: companyId}}).then((vehciles) => {
        vehciles.forEach(vehicle => {
            if (vehicle.id == vehicleId){
                res.status(200).send(vehicle)
            }
        })
    }).catch((err) => {
        console.log(err);
        res.sendStatus(500)
    })
});

// Get a specific engines
router.get('/:companyId/:vehicleId/engines', async (req,res,next) => {
    const companyId = req.params.companyId;
    const vehicleId = req.params.vehicleId;
    console.log(vehicleId)
    Vehicle.findAll({where: {company_id: companyId}}).then((vehciles) => {
        vehciles.forEach(vehicle => {
            if (vehicle.id == vehicleId){
               Engine.findAll({where: {vehicle_id: vehicleId}}).then(result => {
                res.status(200).send(result)
               });
            }
        })
    }).catch((err) => {
        console.log(err);
        res.sendStatus(500)
    })
});

// Create a new vehicle
router.post('/', async (req, res, next) => {
    const companyId = req.body.company_id;
    console.log(companyId)
    const newVehicle = req.body.vehicle;
    const primaryEngine = req.body.primaryEngine;
    const secundaryEngine = req.body.secundaryEngine;

    // Create the vehicle
    Vehicle.create({
        vehicleId: newVehicle.vehicle_ID,
        vehicledescription: newVehicle.vehicledescription,
        vehicleType: newVehicle.vehicleType,
        brand: newVehicle.brand,
        model: newVehicle.model,
        modelYear: newVehicle.modelYear,
        weight: newVehicle.weight,
        company_id: companyId
    }).then(result => {
        console.log(result.id);
        newVehicle.id = result.id;

       vehicleService.produce(newVehicle);

        //Create the primary engine
        Engine.create({
            emissieNorm: primaryEngine.emissieNorm,
            emissionClass:  primaryEngine.emissionClass,
            driveType:  primaryEngine.driveType,
            power:  primaryEngine.power,
            fuel:  primaryEngine.fuel,
            hasSoothFilter:  primaryEngine.hasSoothFilter,
            vehicle_id: newVehicle.id
        }).then(en1 => {
            primaryEngine.id = en1.id
            primaryEngine.vehicle_id = newVehicle.id
            engineService.produce(primaryEngine);
        });

        //Create the secundary engine
        Engine.create({
            emissieNorm: secundaryEngine.emissieNorm,
            emissionClass:  secundaryEngine.emissionClass,
            driveType:  secundaryEngine.driveType,
            power:  secundaryEngine.power,
            fuel:  secundaryEngine.fuel,
            hasSoothFilter:  secundaryEngine.hasSoothFilter,
            vehicle_id: newVehicle.id
        }).then(en2 => {
            secundaryEngine.id = en2.id
            secundaryEngine.vehicle_id = newVehicle.id
            engineService.produce(secundaryEngine);
            res.send(newVehicle);
        })
    })
});

// Update a specific vehicle with his engine(s)
router.put('', async (req, res ,async) => {
    const newVehicle = req.body.vehicle;
    const primaryEngine = req.body.primaryEngine;
    const secundaryEngine = req.body.secundaryEngine;

    // Update Vehicle
    let vehicle = await Vehicle.findAll({where: {id: newVehicle.id}});
    vehicle = newVehicle;
    await Vehicle.upsert(vehicle);

    // Update Engines
    const engines = await Engine.findAll({where: {vehicle_id: newVehicle.id}});
    engines.forEach( async (engine, index) => {
        let temp = await Engine.findAll({where: {id: engine.id}});

        if(index == 0){
            temp = primaryEngine
        }else{
            temp = secundaryEngine;
        }

        await Engine.upsert(temp);
    });

    const msg = {
        v: vehicle,
        prime: primaryEngine,
        sec: secundaryEngine 
    }
    res.status(200).send(msg)
});

// Delete Vehicle and his engine(s)
router.delete('/:vehicleId', async(req, res, next) => {
    const vehicleId = req.params.vehicleId;

    const engines = await Engine.findAll({where: {vehicle_id: vehicleId}});
    console.log("engines: " + engines)
    engines.forEach( async (engine, index) => {
        console.log("engine_id: " + engine.id)
        await Engine.destroy({where: {id: engine.id}});
    });
    await Vehicle.destroy({where: { id: vehicleId }});
    res.status(200).send('Deleted');
});

function checkIfDuplicate(id, type){
    let result = false;
    if(type === 'vehicle'){
        Vehicle.findAll({where: {id: id}})
        .then( () => {
            result =  true;
        })
        .catch(err => {
            console.log('Err: ' + err);
            result = false;
        })
    }else{
        Engine.findAll({where: {id: id}})
        .then( () => {
            result = true;
        })
        .catch(err => {
            console.log('Err: ' + err);
            result = false;
        });
    }

    return result;
}

module.exports = router;